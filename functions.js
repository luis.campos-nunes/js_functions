const similarity = (arr, values) => arr.filter(v => values.includes(v))
const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))
const sortCharactersInString = str => [...str].sort((a, b) => a.localeCompare(b)).join('')
const splitLines = str => str.split(/\r?\n/)
const stripHTMLTags = str => str.replace(/<[^>]*>/g, '')
const sum = (...arr) => [...arr].reduce((acc, val) => acc + val, 0)
const tail = arr => (arr.length > 1 ? arr.slice(1) : arr)
const take = (arr, n = 1) => arr.slice(0, n)
const takeRight = (arr, n = 1) => arr.slice(arr.length - n, arr.length)
const toCurrency = (n, curr, LanguageFormat = undefined) => Intl.NumberFormat(LanguageFormat, { style: 'currency', currency: curr }).format(n)
const toggleClass = (el, className) => el.classList.toggle(className)
const union = (a, b) => Array.from(new Set([...a, ...b]))
const uniqueElements = arr => [...new Set(arr)]
const validateNumber = n => !isNaN(parseFloat(n)) && isFinite(n) && Number(n) == n
const words = (str, pattern = /[^a-zA-Z-]+/) => str.split(pattern).filter(Boolean)
const minN = (arr, n = 1) => [...arr].sort((a, b) => a - b).slice(0, n)
const negate = func => (...args) => !func(...args)
const nodeListToArray = nodeList => [...nodeList]
const radsToDegrees = rad => (rad * 180.0) / Math.PI
const degreesToRads = deg => (deg * Math.PI) / 180.0
const randomHexColorCode = () => '#' +  (Math.random() * 0xfffff * 1000000).toString(16).slice(0, 6)
const randomIntArrayInRange = (min, max, n = 1) => Array.from({ length: n }, () => Math.floor(Math.random() * (max - min + 1)) + min)
const randomIntegerInRange = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min
const redirect = (url, asLink = true) => asLink ? (window.location.href = url) : window.location.replace(url)
const reverseString = str => [...str].reverse().join('')
const round = (n, decimals = 0) => Number(`${Math.round(`${n}e${decimals}`)}e-${decimals}`)
const randomNumberInArray = arr => arr[Math.floor(Math.random() * arr.length)]
const shallowClone = obj => Object.assign({}, obj)
const shuffle = ([...arr]) => { let m = arr.length; while (m) { const i = Math.floor(Math.random() * m--); [arr[m], arr[i]] = [arr[i], arr[m]] } return arr }
const isLowerCase = str => str === str.toLowerCase()
const isPlainObject = val => !!val && typeof val === 'object' && val.constructor === Object
const isUpperCase = str => str === str.toUpperCase()
const last = arr => arr[arr.length - 1]
const matches = (obj, source) => Object.keys(source).every(key => obj.hasOwnProperty(key) && obj[key] === source[key])
const highestNumberInArray = (arr, n = 1) => [...arr].sort((a, b) => b - a).slice(0, n)
const getColonTimeFromDate = date => date.toTimeString().slice(0, 8)
const getDaysDiffBetweenDates = (dateInitial, dateFinal) => (dateFinal - dateInitial) / (1000 * 3600 * 24)
const getType = v => v === undefined ? 'undefined' : v === null ? 'null' : v.constructor.name.toLowerCase()
const head = arr => arr[0]
const httpsRedirect = () => { if (location.protocol !== 'https:') location.replace('https://' + location.href.split('//')[1]) }
const indexOfAll = (arr, val) => arr.reduce((acc, el, i) => (el === val ? [...acc, i] : acc), [])
const intersection = (a, b) => { const s = new Set(b); return a.filter(x => s.has(x)) }
const is = (type, val) => ![, null].includes(val) && val.constructor === type
const isAfterDate = (dateA, dateB) => dateA > dateB
const isBeforeDate = (dateA, dateB) => dateA < dateB
const isAnagram = (str1, str2) => { const normalize = str => str.toLowerCase().replace(/[^a-z0-9]/gi, '').split('').sort().join(''); return normalize(str1) === normalize(str2) }
const deepFlatten = arr => [].concat(...arr.map(v => (Array.isArray(v) ? deepFlatten(v) : v)))
const digitize = n => [...`${n}`].map(i => parseInt(i))
const distance = (x0, y0, x1, y1) => Math.hypot(x1 - x0, y1 - y0)
const filterNonUnique = arr => arr.filter(i => arr.indexOf(i) === arr.lastIndexOf(i))
const findKey = (obj, fn) => Object.keys(obj).find(key => fn(obj[key], key, obj))
const flatten = (arr, depth = 1) => arr.reduce((a, v) => a.concat(depth > 1 && Array.isArray(v) ? flatten(v, depth - 1) : v), [])
const forEachRight = (arr, callback) => arr.slice(0).reverse().forEach(callback)
const all = (arr, fn = Boolean) => arr.every(fn)
const allEqual = arr => arr.every(val => val === arr[0])
const arrayToCSV = (arr, delimiter = ',') => arr.map(v => v.map(x => `"${x}"`).join(delimiter)).join('\n')
const attempt = (fn, ...args) => { try { return fn(...args) } catch (e) { return e instanceof Error ? e : new Error(e) } }
const average = (...nums) => nums.reduce((acc, val) => acc + val, 0) / nums.length
const bifurcateBy = (arr, fn) => arr.reduce((acc, val, i) => (acc[fn(val, i) ? 0 : 1].push(val), acc), [[], []])
const byteSize = str => new Blob([str]).size
const capitalize = ([first, ...rest]) => first.toUpperCase() + rest.join('')
const compact = arr => arr.filter(Boolean)
const countOccurrences = (arr, val) => arr.reduce((a, v) => (v === val ? a + 1 : a), 0)
const dayOfYear = date => Math.floor((date - new Date(date.getFullYear(), 0, 0)) / 1000 / 60 / 60 / 24)
const retrieveObjectInList = (arr, item, value) => arr.splice(arr.map(function(element) {return element[item]}).indexOf(value), 1)